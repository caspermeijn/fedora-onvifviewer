Name:           onvifviewer
Version:        0.10
Release:        1%{?dist}
Summary:        ONVIF camera viewer

#TODO: The WSDL files can be distributed, but are not free software
License:        GPLv3+ and "Redistributable, no modification permitted"
URL:            https://gitlab.com/caspermeijn/onvifviewer
Source0:        https://gitlab.com/caspermeijn/onvifviewer/-/archive/v%{version}/%{name}-v%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  cmake
BuildRequires:  cmake(KDSoap)
BuildRequires:  cmake(KF5CoreAddons)
BuildRequires:  cmake(KF5Kirigami2)
BuildRequires:  cmake(KF5I18n)
BuildRequires:  cmake(KF5XmlGui)
BuildRequires:  cmake(Qt5Core)
BuildRequires:  cmake(Qt5Svg)
BuildRequires:  cmake(Qt5Quick)
BuildRequires:  cmake(Qt5QuickControls2)
BuildRequires:  extra-cmake-modules
BuildRequires:  desktop-file-utils
BuildRequires:  gettext
BuildRequires:  intltool
BuildRequires:  itstool
BuildRequires:  libappstream-glib

Requires:       qt5-qtmultimedia




# BuildRequires:  extra-cmake-modules qt5-qtbase-devel kf5-kcoreaddons-devel kf5-kxmlgui-devel kf5-ki18n-devel kf5-kirigami2-devel qt5-qtsvg-devel qt5-qtdeclarative-devel qt5-qtquickcontrols2-devel qt5-qtmultimedia xorg-x11-server-Xvfb intltool gettext itstool desktop-file-utils libappstream-glib findutils
# Requires:       

%description
The goal of this project is to replace the proprietary app that was needed to 
configure and view my IP camera. The ONVIF protocol can be used to view and 
configure many types of camera's and is a open standard that can be implemented 
using standard SOAP libraries. Using Qt5 for the back-end and Kirigami UI 
framework makes this application a cross-platform solution.

%prep
%autosetup -n %{name}-v%{version}


%build
%cmake .
%make_build


%install
rm -rf %{buildroot}
%make_install
%find_lang %{name}

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/net.meijn.onvifviewer.desktop
appstream-util validate-relax --nonet %{buildroot}%{_metainfodir}/*.appdata.xml


%files -f %{name}.lang
%license LICENSE
%doc README.md
%{_bindir}/onvifviewer
%{_datadir}/metainfo/net.meijn.onvifviewer.appdata.xml
%{_datadir}/applications/net.meijn.onvifviewer.desktop
%{_datadir}/icons/hicolor/*/apps/net.meijn.onvifviewer.png
%{_datadir}/icons/hicolor/scalable/apps/net.meijn.onvifviewer.svg




%changelog
* Tue May 21 2019 Casper Meijn <casper@meijn.net> - 0.10-1
- First onvifviewer package
